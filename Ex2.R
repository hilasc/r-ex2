 install.packages('e1071')
library('e1071')

#text mining package
install.packages('tm')
library('tm')

trainingTitanic<-read.csv('train.csv', stringsAsFactors = FALSE)

testTitanic<-read.csv('test.csv', stringsAsFactors = FALSE)
install.packages('mice')# packege for dealing with missing data.
library('mice')

summary(trainingTitanic)

dim(trainingTitanic)

str(trainingTitanic)

trainingTitanic <- trainingTitanic[,c(2,3,5,6)]

testTitanic <- testTitanic[,c(2,4,5)]

str(trainingTitanic)
str(testTitanic)

trainingTitanic$Survived <- as.factor(trainingTitanic$Survived)

conv_oldMatureChild <-function(x){
  x <-ifelse(x>15,ifelse(x>50,2,1),0)
  x <- factor(x,level=c(2,1,0), labels =c('old','mature','child'))
}

trainingTitanic[4:4] <- apply(trainingTitanic[4:4],MARGIN=1:2,conv_oldMatureChild)
testTitanic[3:3] <- apply(testTitanic[3:3],MARGIN=1:2,conv_oldMatureChild)

trainingTitanic$Pclass <- as.factor(trainingTitanic$Pclass)
testTitanic$Pclass <- as.factor(testTitanic$Pclass)

trainingTitanic$Sex <- as.factor(trainingTitanic$Sex)
testTitanic$Sex <- as.factor(testTitanic$Sex)

trainingTitanic$Age <- as.factor(trainingTitanic$Age)
testTitanic$Age <- as.factor(testTitanic$Age)


str(trainingTitanic)

df_train = as.data.frame(trainingTitanic)
df_test = as.data.frame(testTitanic)
df_test["survived"] <- ''

df_train <- na.omit(df_train)
df_test <- na.omit(df_test)
model <- naiveBayes(df_train[,2:4],df_train[,1])
model

prediction <- predict(model,df_test[-4])
prediction

install.packages('SDMTools')
library(SDMTools)

confusion.matrix(df_train$Survived,prediction)

#confusion matrix on training set

split <- runif(nrow(trainingTitanic)) >0.3
TitanicTrain1 <- trainingTitanic[split,]
TitanicTest1 <- trainingTitanic[!split,]


df_train1 = as.data.frame(TitanicTrain1)
df_test1 = as.data.frame(TitanicTest1)

df_train1 <- na.omit(df_train1) #remove NA
df_test1 <- na.omit(df_test1) #remove NA

model1 <- naiveBayes(df_train1[,2:4],df_train1[,1]) #remove column 1
model1

prediction1 <- predict(model1,df_test1[-1])
prediction1

prediction2 <- predict(model1,df_train1[-1])
prediction2

conv_10 <-function(x){
  x <-ifelse(x==0,0,1)
}

pred01 <- sapply(prediction1,conv_10)
actual01 <- sapply(df_test1$Survived,conv_10)
confusion.matrix(actual01,pred01)

pred02 <- sapply(prediction2,conv_10)
actual02 <- sapply(df_train1$Survived,conv_10)
confusion.matrix(actual02,pred02)
